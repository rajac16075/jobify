import React, { Component } from "react";
import Jobs from "./jobs.json";
import JobBriefList from "./components/JobBriefList";
import SearchBarForName from './components/SearchBarForName';
import SearchBarForLocation from './components/SearchBarForLocation';
import Profile from './components/Profile';
import Companies from './components/Companies'

 import { connect } from 'react-redux';
// import {SearchBarForNameAction,SearchBarForLocationAction} from './action'

class App extends Component {
  
 

  render() {
    

    const jobsToShow = Jobs.filter(job => {
      const { name, location: { city, country } } = job;

      if (this.props.BarName) {
        const nameMatch = name.toLowerCase().includes(this.props.BarName);
        if (!nameMatch) return false;
      }

      if (this.props.BarLocation) {
        const locationMatch = city.toLowerCase().includes(this.props.BarLocation) || country.toLowerCase().includes(this.props.BarLocation);
        if (!locationMatch) return false;
      }

      return true;
    });

    const showJobList = () => {
      if(window.location.pathname === '/') {
        return (
          <div>
            <div className="search-panel" style={{ marginTop: '20px', display: 'inline-block' }}>
              <SearchBarForName  />
              <SearchBarForLocation  />
            </div>
            <a 
              className="btn btn-primary"
              href="/profile"
              target="_blank"
              style={{display: 'inline-block', color:"white", marginLeft: '170px'}}>
                See Your Profile
            </a>
            <a 
              className="btn btn-primary"
              href='/companies'
              target="_blank"
              style={{display: 'inline-block', color:"white", marginLeft: '170px'}}>
                See Companies
            </a>
            <JobBriefList jobs={jobsToShow} />
          </div>
        )
      }
    }

    const showProfile = () => {
      if(window.location.pathname === '/profile') return <Profile />;
    }
    const showCompanies = () => {
      if(window.location.pathname === '/companies') return <Companies />;
    }
 
    return (
      <div className="container">
        {showJobList()}
        {showProfile()}
        {showCompanies()}
      </div>
    );
  }
   

  // handleSearchName =value => {
  //   console.log(value)
  // }

  // handleSearchLocation = searchByJobLocation => this.setState({
  //   searchByJobLocation: searchByJobLocation.toLowerCase()
  // });

}

const mapStatetoProps=(state)=>{
	console.log(state.location.value.toLowerCase())
	return{
    BarName:state.nam.value.toLowerCase(),
    BarLocation:state.location.value.toLowerCase()
	}	
	
}
export default connect(mapStatetoProps)(App);

