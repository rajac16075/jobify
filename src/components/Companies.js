import React, { Component } from 'react'
import Jobs from "../jobs.json";
import JobBriefList from "./JobBriefList";

export class Companies extends Component {

    state={
        tcs :false,
        infosys:false,
        talkval:false,
        main:false
      }

      handleTcs=()=>{
          this.setState({tcs:true,infosys:false,talkval:false,main:true})
      }
      handleInfosys=()=>{
        this.setState({tcs:false,infosys:true,talkval:false,main:true})
    }
    handleTalk=()=>{
        this.setState({tcs:false,infosys:false,talkval:true,main:true})
    }

    render() {

        const jobsToShow = Jobs.filter(job => {
            const { name, location: { city, country },company } = job;
            console.log(job.location.company)
        if(this.state.infosys){
        return job.location.company=='infosys'
        }
        if(this.state.tcs){
            return job.location.company=='tcs'
            }
            if(this.state.talkval){
                return job.location.company=='talkvaley'
                }
        // if(this.state.infosys){
        //    return job.company=='infosys'
        // }
        // if(this.state.tcs){
        //     if(company==='tcs'){
        //         return true
        //     } 
        //    //  const tcscompany=company=='tcs'? true:false
        //    //  if (!tcscompany) return false
        // }
        // if(this.state.talkval){
        //     const talkcompany=company=='talkvaley'? true:false
        //     if  (!talkcompany) return false
        // }
        
        
        })
        return (
            <div className="container mt-n1 .mx-md-n5 ">
            <div className="row justify-content-center">
            <div className="col-3">
            <button type="button"  onClick={this.handleTcs}    className="btn btn-primary ">    INFOSYS</button>
              
            </div>
            <div className="col-3"> <button type="button" onClick={this.handleInfosys} className="btn btn-primary">TCS</button> </div>
            <div className="col-3"><button type="button"  onClick={this.handleTalk} className="btn btn-primary">
                TALK VALLEY LLC.
              </button> </div>
            {this.state.main ?<JobBriefList jobs={jobsToShow}/>:""}
              
            </div>
            </div>
            
        )
    }
}

export default Companies
