import React, { Component } from 'react';
import { connect } from "react-redux";

import { SearchBarForLocationAction } from "../action";

class SearchBarForLocation extends Component {
  

  handleChange = event => {
    this.props.SearchBarForLocationAction(event.target.value);
   }
 

  render() {
    return (
      <span style={{ marginLeft: '30px', display: 'inline-block' }}>
        <label htmlFor="search-where">Where</label>
        <input
          autoComplete="off"
          id="search-where"
          type="text"
          placeholder="Enter city"
          style={{ marginLeft: '10px' }}
          value={this.props.location}
          onChange={this.handleChange}
        />
      </span>
    )
  }
}

const mapToStateProps=state=>{
  console.log(state.location.value)
  return{
    location:state.location.value
  }
}

export default connect(mapToStateProps,{SearchBarForLocationAction})(SearchBarForLocation);