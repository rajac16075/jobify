
import { connect } from 'react-redux';
import {SearchBarForNameAction} from '../action/index'


import React, { Component } from 'react';

class SearchBarForName extends Component {


  handleChange = event => {
   this.props.SearchBarForNameAction(event.target.value);
  }

  render() {
    return (
      <span style={{ marginLeft: '30px', display: 'inline-block' }}>
		
        <label htmlFor="search-where">Where</label>
        <input
		  autoComplete="off"
          id="search-where"
          type="text"
          placeholder="Enter city"
          style={{ marginLeft: '10px' }}
           value={this.props.name}
          onChange={this.handleChange}
        />
      </span>
    )
  }
}

const mapStatetoProps=(state)=>{
	console.log(state.nam.value)
	return{
		name:state.nam.value
	}	
	
}
export default connect(mapStatetoProps,{SearchBarForNameAction})(SearchBarForName);




// function SearchBarForName (props) {
// 	// const [jobName, setJobName] = useState('');
// 	const jobNameInput = useRef(null);

// 	const handleChange = (e) => {
// 		props.SearchBarForNameAction(e.target.value)
// 	}

// 	useEffect(() => {
// 		jobNameInput.current.focus();
// 		props.onChange()
// 	  }, []); // eslint-disable-line react-hooks/exhaustive-deps
	
// 		return (
// 			<span style={{ display: 'inline-block' }}>
// 				<label htmlFor="search-what">What</label>
// 				<input
// 					id="search-what"
// 					type="text"
// 					placeholder="Job title, keywords or company"
// 					style={{ marginLeft: '10px' }}
// 					ref={jobNameInput}
// 					//value={}
// 					onChange={handleChange}
// 				/>
// 			</span>
// 		)
// }
